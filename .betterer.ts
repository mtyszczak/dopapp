// .betterer.ts
import { BettererFileTest, BettererFilePaths, BettererFileTestResult } from "@betterer/betterer";
import { BettererConstraintResult } from "@betterer/constraints";

const countFiles = (pattern:string) =>
  new BettererFileTest(async (filePaths:BettererFilePaths, fileTestResult:BettererFileTestResult) => {
    filePaths.forEach((filePath:string) => {
      fileTestResult
        .addFile( filePath, "" )
        .addIssue( 0, 0, `Files count: ${pattern}` );
    });
  }).include(pattern);

export default {
  "JS -> TS migration": () =>
    countFiles( "src/**/*.js" )
      .goal( r => !r.getIssues().length )
      .constraint((result:BettererFileTestResult, expected:BettererFileTestResult):BettererConstraintResult => {
        if ( result.getIssues().length === expected.getIssues().length )
          return BettererConstraintResult.same;
        if ( result.getIssues().length > expected.getIssues().length )
          return BettererConstraintResult.worse;
        return BettererConstraintResult.better;
      })
};
