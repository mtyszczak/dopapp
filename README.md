# dopapp
Reversed private Orange Flex app API

## Build
In order to build this project you have to execute given commands:
```bash
# Install required binaries (Node.js and Native Package Manager)
sudo apt install -y \
      nodejs \
      npm

# Install TypeScript compiler globally
## You can also install it localy if you want to
sudo npm install -g typescript # ts-node # if you are a developer

# Install project dependencies
## (you have to be in a project root directory)
npm install

# Compile project using TypeScript compiler
## (output will be located in the `data` directory)
npm run build
```

## Testing
### Configure
Before testing the script you have to duplicate the `.env.example` file and name it `.env`.

Then using dotenv syntax fill all of desired fields inside of the `.env` file.
### Running
After [configuring](#Configure) run tests using:
```
npm run test
```

### Cleanup
If you want to clear your current working directory from the untracked, bot data-related directories you can run:
```bash
npm run clear
```
which removes `dist` directories along with their content

## LICENSE
See [LICENSE.md](LICENSE.md)
