/* eslint-disable no-console */
/**
 * @fileoverview dopapp NumberManager tests
 * @author mtyszczak
 */

import NumberManager, { ORDER_CHECK_INTERVAL } from "../src/number-manager";

describe("Test flex number change", () => {

  const manager = new NumberManager( process.env.DOPAPP_USERNAME as string, process.env.DOPAPP_PASSWORD as string );

  let initPhoneNumber:string|undefined;

  it("initiates the number manager", async () => {
    await manager.init();
  });

  it("retrieves the current number", () => {
    initPhoneNumber = manager.getCurrentNumber();
    expect( initPhoneNumber ).toBeDefined();
  });

  it("changes the number", async () => {
    jest.setTimeout( ORDER_CHECK_INTERVAL * 6 ); // 2 minutes

    expect( await manager.rollNumber( 0, ( func, desc, perc ) => {
      console.debug( `[${Math.round(perc).toString()
        .padStart(3)}%] ${func.name} > '${desc}'` );
    } ) ).toBeDefined();
  });

  it("checks if number has been successfully changed", () => {
    expect( manager.getCurrentNumber() ).not.toBe( initPhoneNumber );
  });
});
