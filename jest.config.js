/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  testPathIgnorePatterns: [ "<rootDir>/lib/", "node_modules/" ],
  collectCoverage: true,
  bail: true,
  moduleDirectories: [ "<rootDir>/src", "<rootDir>", "node_modules" ],
  setupFiles: [ "dotenv/config" ],
  setupFilesAfterEnv: [ "./setup-after-env.js" ],
  testRunner: "jest-jasmine2",
  silent: false
};
