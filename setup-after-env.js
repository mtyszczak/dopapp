/**
 * @fileoverview --bail ts-jest flag fail on unit-test level workaround
 */

const failFast = require("jasmine-fail-fast");

const jasmineEnv = jasmine.getEnv()
jasmineEnv.addReporter(failFast.init())
