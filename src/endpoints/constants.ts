/**
 * @fileoverview endpoints constants
 * @author mtyszczak
 */

export const DOPAPP_DOMAIN = "dopapp.pl";

export const PRD_IAM_SUBDOMAIN = "prd-iam";
export const PRD_PROXY_SUBDOMAIN = "prd-proxy";
export const PRD_PORTAL_SUBDOMAIN = "prd-portal";
export const PRD_PUBLIC_GATEWAY_SUBDOMAIN = "prd-public-gateway";
