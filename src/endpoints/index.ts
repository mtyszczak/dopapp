/**
 * @fileoverview dopapp endpoints
 * @author mtyszczak
 */

import * as Iam from "./prd-iam";
import * as Proxy from "./prd-proxy";
import * as Gateway from "./prd-public-gateway";

export default {
  ...Iam.default, ...Proxy.default, ...Gateway.default
};
