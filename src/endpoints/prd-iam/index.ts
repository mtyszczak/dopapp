/**
 * @fileoverview prd-iam endpoints
 * @author mtyszczak
 */

import login from "./login";

export default {
  login
};
