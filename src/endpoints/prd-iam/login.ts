/* eslint-disable camelcase */
/**
 * @fileoverview login to the dopapp
 * @author mtyszczak
 */

import axios from "axios";
import { DOPAPP_DOMAIN, PRD_IAM_SUBDOMAIN } from "../constants";
import { FlexError } from "../../errors";
import { flexErrorString } from "../utils";

export type LoginResponse = {
  "access_token":string;
  "expires_in":number;
  "refresh_expires_in":number;
  "refresh_token":string;
  "token_type":string;
  "not-before-policy":number;
  "session_state":string;
  "scope":string;
};

export default async ( username:string, password:string ):Promise< LoginResponse > => {
  const params = new URLSearchParams();
  params.append("client_id", "dop-mobile-app-v1");
  params.append("client_secret", "ef9f2885-fdf8-466b-80f3-b6fb81510ea4");
  params.append("grant_type", "password");
  params.append("password", password);
  params.append("username", username);
  const response = await axios.post(
    `https://${PRD_IAM_SUBDOMAIN}.${DOPAPP_DOMAIN}/auth/realms/dop/protocol/openid-connect/token`,
    params,
    {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Host": "prd-iam.dopapp.pl",
        "Accept": "application/json, text/plain, */*",
        "Apikey": "8beb7c7a-726c-4fae-aa82-585e295b09e1",
        "User-Agent": "dop/39043 CFNetwork/1327.0.4 Darwin/21.2.0",
        "Accept-Language": "pl-PL,pl;q=0.9"
      }
    }
  );

  if( response.status !== 200 )
    throw new FlexError( flexErrorString( response ) );

  return response.data;
};
