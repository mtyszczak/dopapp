/**
 * @fileoverview prd-proxy endpoints
 * @author mtyszczak
 */

import prepareMACDPrepareData from "./prepare-data";
import getMSISDNList from "./msisdn-list";
import reserveNumber from "./reserve-number";
import sendOrder from "./send-order";

export default {
  prepareMACDPrepareData,
  getMSISDNList,
  reserveNumber,
  sendOrder
};
