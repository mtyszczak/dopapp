/**
 * @fileoverview retrieve the msisdn list
 * @author mtyszczak
 */

import axios from "axios";
import { FlexError } from "../../errors";
import { flexErrorString } from "../utils";
import { DOPAPP_DOMAIN, PRD_PORTAL_SUBDOMAIN, PRD_PROXY_SUBDOMAIN } from "../constants";

type MSISDNListResponse = {
  MSISDN_SI: Array<{
    "value":string;
  }>;
};

export default async ( salesforce:string, msisdnUserId:string ): Promise< MSISDNListResponse > => {
  const response = await axios.post(
    `https://${PRD_PROXY_SUBDOMAIN}.${DOPAPP_DOMAIN}/services/apexrest/vlocity_cmt/v1/GenericInvoke/`,
    {
      sClassName: "vlocity_cmt.IntegrationProcedureService",
      sMethodName: "MSISDN_getMSISDNList",
      input: `{"userId":"${msisdnUserId}"}`,
      options: `{"postTransformBundle":"","preTransformBundle":"","useContinuation":false,"vlcClass":"vlocity_cmt.IntegrationProcedureService"}`,
      iTimeout: 30000,
      label: {
        label: "RetrieveSIMNumber_IP"
      }
    },
    {
      headers: {
        "Host": "prd-proxy.dopapp.pl",
        "Target-Url": `https://${PRD_PORTAL_SUBDOMAIN}.${DOPAPP_DOMAIN}`,
        "Accept": "application/json, text/plain, */*",
        "Origin": "file://",
        "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 15_2_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148",
        "Accept-Language": "pl-PL,pl;q=0.9",
        "Content-Type": "application/json;charset=utf-8",
        "Authorization": `Bearer ${salesforce}`
      }
    }
  );

  if( response.status !== 200 || response.data.error?.toUpperCase() !== "OK" )
    throw new FlexError( flexErrorString( response ) );

  return response.data.IPResult;
};
