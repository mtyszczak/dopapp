/**
 * @fileoverview MACD post reserve sim aka send to managment
 * @author mtyszczak
 */

import axios from "axios";
import { DOPAPP_DOMAIN, PRD_PORTAL_SUBDOMAIN, PRD_PROXY_SUBDOMAIN } from "../constants";
import { FlexError } from "../../errors";
import { flexErrorString } from "../utils";

type SendOrderResponse = undefined;

export default async ( salesforce:string, msisdnUserId:string, number:string ): Promise<SendOrderResponse> => {
  const response = await axios.post(
    `https://${PRD_PROXY_SUBDOMAIN}.${DOPAPP_DOMAIN}/services/apexrest/vlocity_cmt/v1/GenericInvoke/`,
    {
      sClassName: "IPService",
      sMethodName: "runQueueableVip",
      input: `{"processType":"MACDChangeMSISDN","sendToOrderManagement":true,"selectedMSISDN":"${
        number}","isAsyncFlow":true,"coodProductId":"${msisdnUserId}"}"`,
      options: `{"vip":"MACD_CreateAssetBasedOrderNew","preTransformBundle":"","postTransformBundle":`
        + `"","vlcClass":"IPService"}`,
      iTimeout: 30000,
      label: {
        label: "CreateAssetBasedOrderChangePlan_RA"
      }
    },
    {
      headers: {
        "Host": "prd-proxy.dopapp.pl",
        "Target-Url": `https://${PRD_PORTAL_SUBDOMAIN}.${DOPAPP_DOMAIN}`,
        "Accept": "application/json, text/plain, */*",
        "Origin": "file://",
        "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 15_2_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148",
        "Accept-Language": "pl-PL,pl;q=0.9",
        "Content-Type": "application/json;charset=utf-8",
        "Authorization": `Bearer ${salesforce}`
      }
    }
  );

  if( response.status !== 200 || response.data.error?.toUpperCase() !== "OK" )
    throw new FlexError( flexErrorString( response ) );

  return undefined;
};
