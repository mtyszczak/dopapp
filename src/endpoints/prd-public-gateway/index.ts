/**
 * @fileoverview prd-public-gateway endpoints
 * @author mtyszczak
 */

import salesforceLogin from "./salesforce-login";
import getOrders from "./orders";

export default {
  salesforceLogin, getOrders
};
