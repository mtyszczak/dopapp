/**
 * @fileoverview get user orders
 * @author mtyszczak
 */

import axios from "axios";
import { FlexError } from "../../errors";
import { flexErrorString } from "../utils";
import { DOPAPP_DOMAIN, PRD_PUBLIC_GATEWAY_SUBDOMAIN } from "../constants";

export type OrdersResponse = Array<{
  "id":string;
  "productOrderId":string;
  "customerOrderId":string;
  "ordersProcessState":string;
  "mobileStatus":string;
  "name":string;
  "msisdn":string;
  "status":string;
  "fulfillmentStatus":string;
  "date":string;
  "dopUserId":string;
  "orderNumber":string;
  "orderDeliveryType":string;
  "orderPrice":number;
  "paymentStatus":string;
  "simCardList":Array<{
    "id":string;
    "name":string;
    "imsi":string;
    "iccid":string;
    "logisticType":string;
    "primaryCard":boolean;
    "deliveryType":string;
  }>;
  "details": Array<{
    "differenceValue":number;
    "type":string;
    "subType":string;
    "futureValue":number;
    "futurePrice":number;
  }>;
  "isSentToCOOD":boolean;
  "customerVerificationDate":string;
  "itemsPrice":number;
  "orderItem": Array<{
    "action":string;
    "id":string;
    "lineNumber":string;
    "recurringCharge":number;
    "recurringTotal":number;
    "productId":string;
    "oneTimeTotal":number;
    "offer": {
      "billingCatalogItemId":string;
      "name":string;
    },
    "orderItemFulfilmentStatus":string;
  }>;
}>;

export default async ( accessToken:string ):Promise< OrdersResponse > => {
  const response = await axios.get(
    `https://${PRD_PUBLIC_GATEWAY_SUBDOMAIN}.${DOPAPP_DOMAIN}/orders`,
    {
      headers: {
        "Host": "prd-public-gateway.dopapp.pl",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "pl",
        "Apikey": "8beb7c7a-726c-aa82-585e295b09e1",
        "User-Agent": "dop/39043 CFNetwork/1327.0.4 Darwin/21.2.0",
        "Authorization": `Bearer ${accessToken}`
      }
    }
  );

  if( response.status !== 200 )
    throw new FlexError( flexErrorString( response ) );

  return response.data;
};
