/**
 * @fileoverview Salesforce login
 * @author mtyszczak
 */

import axios from "axios";
import { FlexError } from "../../errors";
import { flexErrorString } from "../utils";
import { DOPAPP_DOMAIN, PRD_PUBLIC_GATEWAY_SUBDOMAIN } from "../constants";

type SalesforceLoginResponse = {
  "userId":string;
  "sfdcId":string;
  "tokenType":string;
  "accessToken":string;
};

export default async ( accessToken:string ): Promise< SalesforceLoginResponse > => {

  const response = await axios.get(
    `https://${PRD_PUBLIC_GATEWAY_SUBDOMAIN}.${DOPAPP_DOMAIN}/users/crm`,
    {
      headers: {
        "Host": "prd-public-gateway.dopapp.pl",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "pl",
        "Apikey": "8beb7c7a-726c-4fae-aa82-585e295b09e1",
        "User-Agent": "dop/39043 CFNetwork/1327.0.4 Darwin/21.2.0",
        "Authorization": `Bearer ${accessToken}`
      }
    }
  );

  if( response.status !== 200 )
    throw new FlexError( flexErrorString( response ) );

  return response.data;
};
