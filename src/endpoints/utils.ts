/**
 * @fileoverview utility functions for the endpoints
 * @author mtyszczak
 */

import { AxiosResponse } from "axios";

/**
 * Creates error string from the invalid response
 * @param {AxiosResponse} response axios response
 * @returns {string} stringified error response
 */
export const flexErrorString
  = (response:AxiosResponse) => `Invalid response code: #${response.status}: '${
    typeof response.data === "object" ? JSON.stringify(response.data) : response.data}'`;
