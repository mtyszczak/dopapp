/**
 * @fileoverview dopapp related errors
 * @author mtyszczak
 */

export class FlexError extends Error {
  public name = "FlexError";

  public constructor( msg:string ) { super(msg); }
}
