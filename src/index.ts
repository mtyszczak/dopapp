/**
 * @fileoverview dopapp export file
 * @author mtyszczak
 */

import { FlexError as FE } from "./errors";
export type FlexError = FE;

import NumberManager from "./number-manager";
import TokenManager from './token-manager';
import endpoints from "./endpoints";

export default {
  NumberManager,
  TokenManager,
  endpoints
};
