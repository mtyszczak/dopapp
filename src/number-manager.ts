/* eslint-disable no-promise-executor-return */
/**
 * @fileoverview prd manager for token authentication and requests managment
 * @author mtyszczak
 */

import TokenManager from "./token-manager";
import endpoints from "./endpoints";
import { FlexError } from './errors';

export const ORDER_CHECK_INTERVAL = 20 * 1000; // 20 seconds

export default class NumberManager {
  private tokens      = new TokenManager();

  private flexOrderId:string|undefined;

  private msisdn:string|undefined;

  private msisdnUserId:string|undefined;

  /**
   * Retrieves all of the required tokens and ids from the endpoints
   * @param {string} username user name
   * @param {string} password user password
   * @throws {FlexError} on Orange Flex order id retrieve error
   */
  public constructor(
    private username:string,
    private password:string
    // eslint-disable-next-line no-empty-function
  ) {}

  /**
   * Initializes the connection with the previously set up data
   * @param {?boolean} relogin indicates if the current method should reinvoke itself after the token expiration
   * @returns {Promise<void>}
   * @public
   * @async
   */
  public async init( relogin = true ):Promise<void> {
    if( this.tokens.access.length && this.tokens.refresh.length && this.tokens.salesforce.length )
      throw new FlexError( "Flex has beeen already initialized." );

    const loginResponse = await endpoints.login( this.username, this.password );

    this.tokens.access  = loginResponse.access_token;
    this.tokens.refresh = loginResponse.refresh_token;

    if( !this.tokens.access || !this.tokens.refresh )
      throw new FlexError( "No access tokens present in the response" );

    const salesforceResponse = await endpoints.salesforceLogin( this.tokens.access );
    this.tokens.salesforce = salesforceResponse.accessToken;

    if( !this.tokens.salesforce )
      throw new FlexError( "No salesforce token present in the response" );

    const ordersResponse = await endpoints.getOrders( this.tokens.access );
    for( let i = ordersResponse.length - 1; i >= 0; --i ) {
      if( ordersResponse[i].name !== "Orange Flex" )
        continue;
      if( ordersResponse[i].fulfillmentStatus !== "Completed NetworkCompleted" )
        continue;

      this.msisdn = ordersResponse[i].msisdn;

      if( ordersResponse[i].orderItem.find( item => {
        if( item.offer.billingCatalogItemId === "ConnectivityPackage" )
          return !!(this.flexOrderId = item.productId);

        return false;
      } ) )
        break;
    };
    if( typeof this.flexOrderId === "undefined" || typeof this.msisdn === "undefined" )
      throw new FlexError( "Flex order id or msisdn could not be found. Make sure you own the proper product." );

    this.msisdnUserId = (await endpoints.prepareMACDPrepareData( this.tokens.salesforce, this.flexOrderId as string )).msisdnUserId;

    if( relogin )
      setTimeout( this.init, loginResponse.expires_in * 1000 ); // Usually equals to 7200 * 1000ms -> 2 hours
  }

  /**
   * @returns {string|undefined} current number
   */
  public getCurrentNumber():string|undefined {
    return this.msisdn;
  }

  /**
   * Changes the phone number to the random requested msisdn
   * @param {number} index optional index of the selected number (defaults to 0)
   * @returns {Promise<string>} new msisdn number
   * @return {?(func:Function, description:string, percentage:number)=>void} Callback called before every step in number roll
   * @note callback will be called multiple times. See the percentage function argument
   * @throws {FlexError} on msisdn change error
   * @async
   * @public
   */
  // eslint-disable-next-line @typescript-eslint/ban-types
  public async rollNumber( index = 0, callback?:(func:Function, description:string, percentage:number)=>void ):Promise<string> {
    if( typeof callback !== "undefined" )
      callback( endpoints.getMSISDNList, "Get list of the available numbers", (100 / 5 * 1) );
    const numbers = (await endpoints.getMSISDNList( this.tokens.salesforce, this.msisdnUserId as string )).MSISDN_SI.map( val => val.value );
    if( typeof numbers[index] === "undefined" )
      throw new FlexError( `MSISDN with the given index not found in the received MSISDN list` );
    if( typeof callback !== "undefined" )
      callback( endpoints.reserveNumber, `Reserve number: ${numbers[index]}`, (100 / 5 * 2) );
    await endpoints.reserveNumber( this.tokens.salesforce, this.msisdnUserId as string, numbers[index] );
    if( typeof callback !== "undefined" )
      callback( endpoints.sendOrder, "Send the new order", (100 / 5 * 3) );
    await endpoints.sendOrder( this.tokens.salesforce, this.flexOrderId as string, numbers[index] );

    if( typeof callback !== "undefined" )
      callback( endpoints.getOrders, "Wait until order is confirmed", (100 / 5 * 4) );
    let ordersResponse = await endpoints.getOrders( this.tokens.access );
    while( ordersResponse[ordersResponse.length - 1].msisdn !== numbers[index]
      || ordersResponse[ordersResponse.length - 1].fulfillmentStatus !== "Completed NetworkCompleted"
    ) {
      if( typeof callback !== "undefined" )
        callback( endpoints.getOrders, `Wait until order is confirmed for '${
          ordersResponse[ordersResponse.length - 1].msisdn}': '${
          ordersResponse[ordersResponse.length - 1].fulfillmentStatus}'`, ( 100 / 5 * 4 ) );
      await new Promise( resTime => void (setTimeout( resTime, ORDER_CHECK_INTERVAL )) );
      ordersResponse = await endpoints.getOrders( this.tokens.access );
    }

    this.msisdn = numbers[index];

    if( typeof callback !== "undefined" )
      callback( endpoints.getMSISDNList, "Done", (100 / 5 * 5) );

    return numbers[index];
  }
};
