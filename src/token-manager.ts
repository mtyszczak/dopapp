/**
 * @fileoverview Basic token manager
 * @author mtyszczak
 */

export default class TokenManager {

  /**
   * Default user access token for the Bearer authentication
   * @default ""
   */
  public access = "";

  /**
   * Default user access refresh token
   * @default ""
   */
  public refresh = "";

  /**
   * Default user access salesforce token for the dopapp proxy Bearer authentication
   * @default ""
   */
  public salesforce = "";

  // eslint-disable-next-line no-empty-function
  public constructor() {}
}
